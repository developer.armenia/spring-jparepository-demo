package am.egs.lessons.jpademo.repository;

import am.egs.lessons.jpademo.entity.Contact_Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface ContactNoteRepository extends JpaRepository<Contact_Note, Long> {

}
